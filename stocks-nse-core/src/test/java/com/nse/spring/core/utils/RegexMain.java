package com.nse.spring.core.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.util.CollectionUtils;

import reactor.core.publisher.Mono;

public class RegexMain {

	public static void main(String[] args) {
		Pattern p1 = Pattern.compile("(Dividend - Rs )");
		Pattern p2 = Pattern.compile("\\d+.\\d+");
		Matcher m1 = p1.matcher("Annual General Meeting/Dividend - Rs 1.2 Per Share");
		Matcher m2 = p2.matcher("Annual General Meeting/Dividend - Rs 951.290Per Share");
		//System.out.println(matcher.find());
		while(m1.find() && m2.find()) {
			System.out.println(m2.group());
		}
		List<Integer> lists = new ArrayList<Integer>();
		//lists.add(2);
		Mono ml1 = Mono.just(lists)
		.filter(p-> CollectionUtils.isEmpty(lists))
		.map(ma -> {
			System.out.println("EMpty hai bhai Empty");
			return ma;
		});
		Mono ml2 = Mono.just(lists).filter(p->!CollectionUtils.isEmpty(lists))
				.map(ma -> {
					System.out.println("Full ho gaya hai bhai full");
					return ma;
				});
		ml2.and(ml1).block();
	}

}
