package com.nse.spring.core.utils;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;

public class DateUtilsTest {

	@Test
	public void diffInMinute() {
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		startDate.setTime(new Date());
		endDate.setTime(new Date());
		endDate.add(Calendar.MINUTE, 5);
		long diff = DateUtils.diffMinute(startDate.getTime(), endDate.getTime());
		assertEquals(1, diff);
	}
}
