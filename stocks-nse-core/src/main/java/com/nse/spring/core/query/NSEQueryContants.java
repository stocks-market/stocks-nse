package com.nse.spring.core.query;

public interface NSEQueryContants {
	
	
	String NSE_INDICES_META_CHECK = "SELECT * FROM NSE_INDICES_META meta WHERE meta.symbol=:symbol AND to_char(meta.trade_Date, 'DD-MMM-YYYY')=:tradeDate";
	
	String NSE_INDICES_META_TRADEDATE_CHECK = "SELECT * FROM NSE_INDICES_META meta WHERE to_char(meta.trade_Date, 'DD-MMM-YYYY')=:tradeDate";
	
	String NSE_INDICES_EQUITY_TRADEDATE_CHECK = "SELECT * FROM NSE_INDICEX_EQUITY meta WHERE to_char(meta.trade_Date, 'DD-MMM-YYYY')=:tradeDate";
	
	String NSE_EQUITY_INDICES_STOCKS_TRADEDATE_CHECK = "SELECT * FROM NSE_INDICES_STOCKS meta WHERE to_char(meta.trade_Date, 'DD-MMM-YYYY')=:tradeDate";
}
