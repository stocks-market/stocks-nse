package com.nse.spring.core.utils;

public class NSEContants {
	
	public static final String NSE_JWT_ISSUER = "api.nse";
	
	public static final String NSE_JWT_AUDIENCE = NSE_JWT_ISSUER;
}
