package com.nse.spring.core.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.springframework.http.ResponseCookie;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;

import com.nse.spring.core.domain.AppsAuth;
import com.nse.spring.core.repository.AppsAuthRepository;

import reactor.core.publisher.Flux;

public final class RestFluxUtils {

	public static WebClient webClient(String baseUrl) {
		return WebClient.builder().baseUrl(baseUrl).build();
	}

	public static Flux<AppsAuth> nseCookie(String baseUrl) {
		return RestFluxUtils.webClient(baseUrl).get().exchange().map(res -> res.cookies()).map(res -> {
			List<AppsAuth> appsAuths = new ArrayList<AppsAuth>();
			if (CollectionUtils.isEmpty(res)) {
				return appsAuths;
			}
			Set<String> keys = res.keySet();
			for (String key : keys) {
				ResponseCookie cookie = res.getFirst(key);
				AppsAuth appsAuth = new AppsAuth();
				appsAuth.setActive(true);
				appsAuth.setName(AppsContants.APPS_NSE);
				appsAuth.setCreatedDate(new Date());
				appsAuth.setKey(cookie.getName());
				appsAuth.setValue(cookie.getValue() != null ? cookie.getValue().getBytes() : null);
				appsAuths.add(appsAuth);
			}
			return appsAuths;
		}).flatMapIterable(Function.identity());
	}

	public static Flux<AppsAuth> nseCookie(String baseUrl, AppsAuthRepository appsAuthRepository) {
		List<AppsAuth> appsAuths = appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE);
		if (!CollectionUtils.isEmpty(appsAuths)) {
			for (AppsAuth appsAuth : appsAuths) {
				appsAuth.setActive(false);
				appsAuthRepository.save(appsAuth);
			}
		}
		return RestFluxUtils.nseCookie(baseUrl).map(appsAuth -> appsAuthRepository.save(appsAuth));
	}
}
