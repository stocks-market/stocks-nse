package com.nse.spring.core.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = { "id" })
@JsonInclude(value = Include.NON_NULL)
public class NSEInsider implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1054028853669925780L;

	private Integer id;

	private String symbol;

	private String acqMode;

	private String acqName;

	private String secAcq;

	private String secVal;
	
	private String secType;

	private String pid;

	private String personCategory;

	private String exchange;

	private String tdpTransactionType;

	private String securitiesTypePost;

	private String companyName;

	private String xbrl;

	private String intimDt;

	private String anex;

	private String buyValue;

	private String buyQuantity;

	private String sellValue;

	private String sellQuantity;

	private String derivativeType;

	private String acqfromDt;

	private String acqtoDt;

	private String afterAcqSharesNo;

	private String afterAcqSharesPer;

	private String befAcqSharesNo;

	private String befAcqSharesPer;

	private String remarks;
}
