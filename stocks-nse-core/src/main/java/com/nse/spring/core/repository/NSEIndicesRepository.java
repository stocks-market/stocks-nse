package com.nse.spring.core.repository;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nse.spring.core.domain.NSEIndices;

public interface NSEIndicesRepository extends JpaRepository<NSEIndices, Serializable> {

	Optional<NSEIndices> findBySymbol(String symbol);
}
