package com.nse.spring.core.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonInclude(value = Include.NON_NULL)
@EqualsAndHashCode(of = { "tradeDate", "symbol", "series" })
public class NSEEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5185988763652651669L;

	private Integer id;

	private String name;

	private String symbol;

	private String key;

	private String value;

	private String faceValue;

	private String description;

	private String tradeDate;

	private String recordDate;

	private String series;

	private String isIn;

	private String startDate;

	private String endDate;

	private String purpose;
}
