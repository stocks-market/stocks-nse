package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(of = {"symbol", "isIn", "series"})
@JsonInclude(value = Include.NON_NULL)
public class NSEStocks implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6338915913320841747L;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "symbol")
	private String symbol;
	
	@Column(name = "series")
	private String series;
	
	@Column(name = "isIn")
	private String isIn;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "industry")
	private String industry;
	
	@Column(name = "listedDate")
	private Date listedDate;
	
	@Column(name = "sectorIndex")
	private String sectorIndex;
	
	@Column(name = "caSec")
	private boolean caSec;
	
	@Column(name = "debitSec")
	private boolean debitSec;
	
	@Column(name = "delist")
	private boolean delist;
	
	@Column(name = "etfSec")
	private boolean etfSec;
	
	@Column(name = "fnoSec")
	private boolean fnoSec;
	
	@Column(name = "slbSec")
	private boolean slbSec;
	
	@Column(name = "suspend")
	private boolean suspend;
	
	@Column(name = "top10")
	private boolean top10;	
}
