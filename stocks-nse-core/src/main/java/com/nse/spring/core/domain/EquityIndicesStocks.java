package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = { "equity", "indices", "index" })
@Table(name = "NSE_INDICES_STOCKS")
@JsonInclude(value = Include.NON_NULL)
public class EquityIndicesStocks implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1117312208113047475L;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;
	
	@Column(name = "equity")
	private String equity;
	
	@Column(name = "position_Id")
	private Integer positionId;
	
	@Column(name = "series")
	private String series;
	
	@Column(name = "indices")
	private String indices;
	
	@Column(name = "stock_index")
	private String index;
	
	@Column(name = "tradeDate")
	private Date tradeDate;
}
