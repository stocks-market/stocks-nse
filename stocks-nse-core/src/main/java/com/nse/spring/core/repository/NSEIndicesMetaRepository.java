package com.nse.spring.core.repository;

import static com.nse.spring.core.query.NSEQueryContants.NSE_INDICES_META_CHECK;
import static com.nse.spring.core.query.NSEQueryContants.NSE_INDICES_META_TRADEDATE_CHECK;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nse.spring.core.domain.NSEIndicesMeta;

public interface NSEIndicesMetaRepository extends JpaRepository<NSEIndicesMeta, Serializable> {

	@Query(value = NSE_INDICES_META_CHECK, nativeQuery = true)
	Optional<NSEIndicesMeta> findBySymbolAndTradeDate(@Param("symbol") String symbol, @Param("tradeDate") String tradeDate);

	@Query(value = NSE_INDICES_META_TRADEDATE_CHECK, nativeQuery = true)
	List<NSEIndicesMeta> findByTradeDate(@Param("tradeDate") String tradeDate);

}
