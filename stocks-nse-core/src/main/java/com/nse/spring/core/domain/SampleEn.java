package com.nse.spring.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(of = { "symbol", "name", "indices" })
@Table(name = "NSE_INDICES")
@JsonInclude(value = Include.NON_NULL)
public class SampleEn implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3608102576523731059L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SYMBOL")
	private String symbol;

	@Column(name = "INDICES")
	private String indices;
}
