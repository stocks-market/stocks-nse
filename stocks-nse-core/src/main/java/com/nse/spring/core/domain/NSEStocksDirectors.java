package com.nse.spring.core.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode(of = { "symbol", "tradeDate" })
@JsonInclude(value = Include.NON_NULL)
public class NSEStocksDirectors implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4692704074696964337L;

	private Integer id;

	private String symbol;
	
	private String companyName;

	private String tradeDate;

	private String name;

	private String category;

	private String title;

	private Double tenure;
	
	private Double nosMemberShip;
	
	private Double nosDirShip;
	
	private Double nosChairPerson;

	private String appointmentDate;

	private String cessationDate;

	private String exDate;
	
	private String cgDate;
	
	private String recordId;
	
	private String xbrl;
}
