package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class NSETrades implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -780109319598045709L;
	
	private Integer id;
	
	private String symbol;
	
	private String series;
	
	private Date tradeDate;
	
	private Double perPriceBand;
	
	private Double ltp;
	
	private Double tradeValue;
	
	private Double volume;
	
	private Double deliverQty;
	
	private Double perDelivery;
}
