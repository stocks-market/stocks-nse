package com.nse.spring.core.repository;

import static com.nse.spring.core.query.NSEQueryContants.NSE_INDICES_EQUITY_TRADEDATE_CHECK;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nse.spring.core.domain.NSEIndexEquity;

public interface NSEIndexEquityRepository extends JpaRepository<NSEIndexEquity, Serializable> {
	
	@Query(value = NSE_INDICES_EQUITY_TRADEDATE_CHECK, nativeQuery = true)
	List<NSEIndexEquity> findByTradeDate(String tradeDate);
}
