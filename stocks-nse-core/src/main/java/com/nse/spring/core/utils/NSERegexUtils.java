package com.nse.spring.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NSERegexUtils {

	public static final String NSE_DIVIDEND_PRICE_REGEX = "\\d+.\\d+";

	public static final String NSE_ALPHA_NUMERIC_REGEX = "^[a-zA-Z0-9]+$";

	public static final String NSE_AGM_REGEX = "(Annual General Meeting)";

	public static final String NSE_DIVIDEND_REGEX = "(Dividend - Rs )";
	
	public static final String NSE_DATE_FORMAT_REGEX = "^(1[0-2]|0[1-9])(3[01]|[12][0-9]|0[1-9])[0-9]{4}$";

	public static final boolean isRegex(String regex, String description) {
		Pattern dividend = Pattern.compile(regex);
		Matcher dividendMatch = dividend.matcher(description);
		return dividendMatch.find();
	}

	public static final boolean isAGM(String description) {
		Pattern dividend = Pattern.compile(NSE_AGM_REGEX);
		Matcher dividendMatch = dividend.matcher(description);
		return dividendMatch.find();
	}

	public static final String dividend(String description) {
		Pattern dividend = Pattern.compile(NSE_DIVIDEND_PRICE_REGEX);
		Matcher dividendMatch = dividend.matcher(description);
		String value = null;
		while (isRegex(NSE_DIVIDEND_REGEX, description) && dividendMatch.find()) {
			value = dividendMatch.group();
			break;
		}
		return value;
	}
}
