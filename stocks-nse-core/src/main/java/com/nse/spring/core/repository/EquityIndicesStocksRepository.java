package com.nse.spring.core.repository;

import static com.nse.spring.core.query.NSEQueryContants.NSE_EQUITY_INDICES_STOCKS_TRADEDATE_CHECK;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nse.spring.core.domain.EquityIndicesStocks;

public interface EquityIndicesStocksRepository extends JpaRepository<EquityIndicesStocks, Serializable> {
	
	@Query(value = NSE_EQUITY_INDICES_STOCKS_TRADEDATE_CHECK, nativeQuery = true)
	List<EquityIndicesStocks> findByTradeDate(@Param("tradeDate") String tradeDate);
}
