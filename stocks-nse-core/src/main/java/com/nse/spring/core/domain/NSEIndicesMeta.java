package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "NSE_INDICES_META")
@Data
//@EqualsAndHashCode(exclude = { "id" })
@EqualsAndHashCode(of = { "symbol", "tradeDate" })
@JsonInclude(value = Include.NON_NULL)
public class NSEIndicesMeta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4740061212364097197L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "symbol")
	private String symbol;

	@Column(name = "indices")
	private String indices;

	@Column(name = "advance")
	private Double advance;

	@Column(name = "decline")
	private Double decline;

	@Column(name = "unchange")
	private Double unchange;

	@Column(name = "tradeDate")
	private Date tradeDate;

	@Column(name = "pe")
	private Double pe;

	@Column(name = "pb")
	private Double pb;

	@Column(name = "ltp")
	private Double ltp;

	@Column(name = "variation")
	private Double variation;

	public NSEIndices nseIndices() {
		NSEIndices indices = new NSEIndices();
		indices.setId(getId());
		indices.setIndices(getIndices());
		indices.setName(getName());
		indices.setSymbol(getSymbol());
		return indices;
	}
}
