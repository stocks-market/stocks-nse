package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "NSE_INDICEX_EQUITY")
@Data
@EqualsAndHashCode(of = { "symbol", "tradeDate" })
@JsonInclude(value = Include.NON_NULL)
public class NSEIndexEquity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3160424218580234443L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;

	@Column(name = "position_Id")
	private Integer positionId;

	@Column(name = "indices")
	private String indices;

	@Column(name = "symbol")
	private String symbol;

	@Column(name = "series")
	private String series;

	@Column(name = "trade_date")
	private Date tradeDate;

	@Column(name = "ltp")
	private Double ltp;

	@Column(name = "ffmc")
	private Double ffmc;

	@Column(name = "open_price")
	private Double open;

	@Column(name = "close_price")
	private Double close;

	@Column(name = "high_price")
	private Double high;

	@Column(name = "low_price")
	private Double low;

	@Column(name = "avg_price")
	private Double avg;

	@Column(name = "prevClose")
	private Double prevClose;

	@Column(name = "tradeQty")
	private Double tradeQty;

	@Column(name = "turnValue")
	private Double turnValue;

	@Column(name = "nosTrade")
	private Double nosTrade;

	@Column(name = "deliverableQty")
	private Double deliverableQty;

	@Column(name = "perdeliverable")
	private Double perdeliverable;

	@Column(name = "dVolatility")
	private Double dVolatility;

	@Column(name = "eVolatility")
	private Double eVolatility;

	@Column(name = "logReturn")
	private Double logReturn;

	@Column(name = "timeFrame")
	private String timeFrame = "DAY";

	public Double change() {
		if (changes()) {
			return 0.0;
		}
		return prevClose - close;
	}

	public Double perChange() {
		if (changes()) {
			return 0.0;
		}
		return (change() * 100) / close;
	}

	private boolean changes() {
		return prevClose == null || close == null || close.equals(0);
	}
}
