package com.nse.spring.core.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;

public class JsonPathUtils {

	public static DocumentContext jsonData(String jsonData) {
		Configuration configuration = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
		DocumentContext context = JsonPath.parse(jsonData, configuration);
		return context;
	}

	public static DocumentContext list() {
		return list(Collections.emptyList());
	}

	public static DocumentContext list(List<Object> list) {
		Gson gson = new Gson();
		List<Object> jsonData = list;
		if (list == null) {
			jsonData = new ArrayList<Object>();
		}
		DocumentContext doc = JsonPath.parse(gson.toJson(jsonData));
		return doc;
	}

	public static DocumentContext map() {
		Gson gson = new Gson();
		DocumentContext doc = JsonPath.parse(gson.toJson(Collections.EMPTY_MAP));
		return doc;
	}

	public static DocumentContext set() {
		Gson gson = new Gson();
		DocumentContext doc = JsonPath.parse(gson.toJson(Collections.EMPTY_SET));
		return doc;
	}

	public static int length(DocumentContext context, String value) {
		List list = context.read(value, List.class);
		return CollectionUtils.isEmpty(list) ? 0 : list.size();
	}

	public static boolean isEmpty(String jsonData) {
		String json = StringUtils.trimAllWhitespace(jsonData);
		boolean jsonFlag = StringUtils.hasLength(json) ? "-".equals(json) || "{}".equalsIgnoreCase(json) || "[]".equalsIgnoreCase(json)
				: false;
		return StringUtils.isEmpty(jsonData) || "".equalsIgnoreCase(json) || jsonFlag;
	}
	
	public static Double getDouble(DocumentContext context, String jsonPath) {
		String jsonData = context.read(jsonPath, String.class);
		if(isEmpty(jsonData)) {
			return Double.parseDouble("0");
		}
		return Double.parseDouble(jsonData);
	}
	
	public static Double getDouble(String jsonPath) {
		if(isEmpty(jsonPath)) {
			return Double.parseDouble("0");
		}
		return Double.parseDouble(jsonPath);
	}
	
	public static Date getDate(DocumentContext context, String format) {
		String timeStamp = context.read("$.timestamp", String.class);
		return DateUtils.getDate(timeStamp, format);
	}
}
