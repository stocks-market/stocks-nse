package com.nse.spring.core.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jayway.jsonpath.DocumentContext;
import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEComplaint;
import com.nse.spring.core.domain.NSEEvent;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.core.domain.NSEInsider;
import com.nse.spring.core.domain.NSEStocks;
import com.nse.spring.core.domain.NSEStocksDirectors;
import com.nse.spring.core.domain.NSEVolatility;

public class NSEDomainUtils {

	public static final String DATE_FORMAT_HRS = "dd-MMM-yyy HH:mm:ss";

	public static final String DATE_FORMAT = "dd-MMM-yyyy";

	public static NSEStocks nseStock(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		NSEStocks nseStock = new NSEStocks();
		nseStock.setSymbol(response.read("$.metadata.symbol", String.class));
		nseStock.setSeries(response.read("$.metadata.series", String.class));
		nseStock.setIsIn(response.read("$.metadata.isin", String.class));
		nseStock.setName(response.read("$.info.companyName", String.class));
		nseStock.setStatus(response.read("$.metadata.status", String.class));
		nseStock.setIndustry(response.read("$.metadata.industry", String.class));
		nseStock.setSectorIndex(response.read("$.metadata.pdSectorInd", String.class));
		nseStock.setCaSec(response.read("$.info.isCASec", Boolean.class));
		nseStock.setDebitSec(response.read("$.info.isDebtSec", Boolean.class));
		nseStock.setDelist(response.read("$.info.isDelisted", Boolean.class));
		nseStock.setEtfSec(response.read("$.info.isETFSec", Boolean.class));
		nseStock.setFnoSec(response.read("$.info.isFNOSec", Boolean.class));
		nseStock.setSlbSec(response.read("$.info.isSLBSec", Boolean.class));
		nseStock.setSuspend(response.read("$.info.isSuspended", Boolean.class));
		nseStock.setTop10(response.read("$.info.isTop10", Boolean.class));
		Date listedDate = DateUtils.getDate(response.read("$.metadata.listingDate", String.class), DATE_FORMAT);
		nseStock.setListedDate(listedDate);
		return nseStock;
	}
	
	public static List<EquityIndicesStocks> equityIndicesStocks(String jsonData, String indices) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<EquityIndicesStocks> nseStocks = new ArrayList<EquityIndicesStocks>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.data");
		Date tradeDate = JsonPathUtils.getDate(response, DATE_FORMAT_HRS);
		for (int i = 0; i < length; i++) {
			EquityIndicesStocks stocks = new EquityIndicesStocks();
			stocks.setPositionId(i);
			stocks.setEquity(response.read("$.data["+i+"].symbol", String.class));
			stocks.setSeries(response.read("$.data["+i+"].series", String.class));
			stocks.setIndices(indices);
			stocks.setTradeDate(tradeDate);
			nseStocks.add(stocks);
		}
		return nseStocks;
	}

	public static List<NSEInsider> stocksInsiders(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEInsider> nseInsiders = new ArrayList<NSEInsider>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.corporate.insiderTrading");
		for (int i = 0; i < length; i++) {
			NSEInsider nseInsider = new NSEInsider();
			nseInsider.setSymbol(response.read("$.corporate.insiderTrading[" + i + "].symbol", String.class));
			nseInsider.setCompanyName(response.read("$.corporate.insiderTrading[" + i + "].companyName", String.class));
			nseInsider.setAnex(response.read("$.corporate.insiderTrading[" + i + "].anex", String.class));
			nseInsider.setAcqName(response.read("$.corporate.insiderTrading[" + i + "].acqName", String.class));
			nseInsider.setSecAcq(response.read("$.corporate.insiderTrading[" + i + "].secAcq", String.class));
			nseInsider.setSecType(response.read("$.corporate.insiderTrading[" + i + "].secType", String.class));
			nseInsider.setSecVal(response.read("$.corporate.insiderTrading[" + i + "].secVal", String.class));
			nseInsider.setTdpTransactionType(response.read("$.corporate.insiderTrading[" + i + "].tdpTransactionType", String.class));
			nseInsider.setXbrl(response.read("$.corporate.insiderTrading[" + i + "].xbrl", String.class));
			nseInsider.setIntimDt(response.read("$.corporate.insiderTrading[" + i + "].date", String.class));
			nseInsiders.add(nseInsider);
		}
		return nseInsiders;
	}

	public static List<NSEStocksDirectors> stocksDirectors(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEStocksDirectors> nseStocksDirectors = new ArrayList<NSEStocksDirectors>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int lengthMeta = JsonPathUtils.length(response, "$.corporate.governance");
		for (int i = 0; i < lengthMeta; i++) {
			int lengthDetail = JsonPathUtils.length(response, "$.corporate.governance["+ i +"].detail");
			for(int j = 0; j < lengthDetail; j++) {
				NSEStocksDirectors stocksDirector = new NSEStocksDirectors();
				stocksDirector.setSymbol(response.read("$.corporate.companyDirectory[0].symbol", String.class));
				stocksDirector.setCompanyName(response.read("$.corporate.companyDirectory[0].smName", String.class));
				stocksDirector.setTradeDate(response.read("$.corporate.governance[" + i + "].metadata.date", String.class));
				stocksDirector.setRecordId(response.read("$.corporate.governance[" + i + "].metadata.recordId", String.class));
				stocksDirector.setXbrl(response.read("$.corporate.governance[" + i + "].metadata.xbrl", String.class));
				stocksDirector.setCgDate(response.read("$.corporate.governance[" + i + "].metadata.cgTimeStamp", String.class));
				stocksDirector.setAppointmentDate(response.read("$.corporate.governance[" + i + "].detail[" + j + "].dateOfAppointment", String.class));
				stocksDirector.setTitle(response.read("$.corporate.governance[" + i + "].detail[" + j + "].title", String.class));
				stocksDirector.setName(response.read("$.corporate.governance[" + i + "].detail[" + j + "].directorName", String.class));
				stocksDirector.setTenure(JsonPathUtils.getDouble(response, "$.corporate.governance[" + i + "].detail[" + j + "].tenure"));
				stocksDirector.setCategory(response.read("$.corporate.governance[" + i + "].detail[" + j + "].category", String.class));
				stocksDirector.setExDate(response.read("$.corporate.governance[" + i + "].detail[" + j + "].date", String.class));
				stocksDirector.setCessationDate(response.read("$.corporate.governance[" + i + "].detail[" + j + "].dateOfCessation", String.class));
				stocksDirector.setNosChairPerson(JsonPathUtils.getDouble(response, "$.corporate.governance[" + i + "].detail[" + j + "].noOfDirShp"));
				stocksDirector.setNosMemberShip(JsonPathUtils.getDouble(response, "$.corporate.governance[" + i + "].detail[" + j + "].noOfMemberships"));
				stocksDirector.setNosDirShip(JsonPathUtils.getDouble(response, "$.corporate.governance[" + i + "].detail[" + j + "].noOfDirShp"));
				nseStocksDirectors.add(stocksDirector);
			}
		}
		return nseStocksDirectors;
	}

	public static List<NSEEvent> stocksCorporateActions(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEEvent> nseEvents = new ArrayList<NSEEvent>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.corporate.corporateActions");
		for (int i = 0; i < length; i++) {
			NSEEvent nseEvent = new NSEEvent();
			nseEvent.setSymbol(response.read("$.corporate.companyDirectory[0].symbol", String.class));
			nseEvent.setName(response.read("$.corporate.companyDirectory[0].smName", String.class));
			nseEvent.setSeries(response.read("$.corporate.corporateActions[" + i + "].series", String.class));
			nseEvent.setRecordDate(response.read("$.corporate.corporateActions[" + i + "].recDate", String.class));
			nseEvent.setTradeDate(response.read("$.corporate.corporateActions[" + i + "].exDate", String.class));
			nseEvent.setFaceValue(response.read("$.corporate.corporateActions[" + i + "].faceVal", String.class));
			nseEvent.setDescription(response.read("$.corporate.corporateActions[" + i + "].subject", String.class));
			nseEvent.setStartDate(response.read("$.corporate.corporateActions[" + i + "].bcEndDate", String.class));
			nseEvent.setEndDate(NSERegexUtils.dividend(nseEvent.getDescription()));
			String dividend = NSERegexUtils.dividend(nseEvent.getDescription());
			if (dividend != null) {
				nseEvent.setKey("DIVIDEND");
				nseEvent.setValue(dividend);
			} else if (NSERegexUtils.isAGM(nseEvent.getDescription())) {
				nseEvent.setKey("AGM");
				nseEvent.setValue(nseEvent.getDescription());
			}
			nseEvents.add(nseEvent);
		}
		return nseEvents;
	}

	public static List<NSEEvent> corporateActions(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEEvent> nseEvents = new ArrayList<NSEEvent>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$");
		for (int i = 0; i < length; i++) {
			NSEEvent nseEvent = new NSEEvent();
			nseEvent.setSymbol(response.read("$[" + i + "].symbol", String.class));
			nseEvent.setSeries(response.read("$[" + i + "].series", String.class));
			nseEvent.setName(response.read("$[" + i + "].comp", String.class));
			nseEvent.setIsIn(response.read("$[" + i + "].isin", String.class));
			nseEvent.setFaceValue(response.read("$[" + i + "].faceVal", String.class));
			nseEvent.setDescription(response.read("$[" + i + "].subject", String.class));
			String dividend = NSERegexUtils.dividend(nseEvent.getDescription());
			if (dividend != null) {
				nseEvent.setKey("DIVIDEND");
				nseEvent.setValue(dividend);
			} else if (NSERegexUtils.isAGM(nseEvent.getDescription())) {
				nseEvent.setKey("AGM");
				nseEvent.setValue(nseEvent.getDescription());
			}
			String startDate = response.read("$[" + i + "].bcStartDate", String.class);
			String endDate = response.read("$[" + i + "].bcEndDate", String.class);
			String tradeDate = response.read("$[" + i + "].exDate", String.class);
			String recordDate = response.read("$[" + i + "].recDate", String.class);
			nseEvent.setStartDate(startDate);
			nseEvent.setEndDate(endDate);
			nseEvent.setTradeDate(tradeDate);
			nseEvent.setRecordDate(recordDate);
			nseEvents.add(nseEvent);
		}
		return nseEvents;
	}

	public static List<NSEIndicesMeta> nseIndicesMeta(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEIndicesMeta> nseIndices = new ArrayList<NSEIndicesMeta>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.data");
		Date tradeDate = JsonPathUtils.getDate(response, DATE_FORMAT_HRS);
		for (int i = 0; i < length; i++) {
			NSEIndicesMeta indices = new NSEIndicesMeta();
			indices.setSymbol(response.read("$.data[" + i + "].indexSymbol", String.class));
			indices.setName(response.read("$.data[" + i + "].index", String.class));
			indices.setIndices(response.read("$.data[" + i + "].key", String.class));
			indices.setPe(JsonPathUtils.getDouble(response, "$.data[" + i + "].pe"));
			indices.setPb(JsonPathUtils.getDouble(response, "$.data[" + i + "].pb"));
			indices.setUnchange(JsonPathUtils.getDouble(response, "$.data[" + i + "].unchanged"));
			indices.setVariation(JsonPathUtils.getDouble(response, "$.data[" + i + "].variation"));
			indices.setLtp(JsonPathUtils.getDouble(response, "$.data[" + i + "].last"));
			indices.setTradeDate(tradeDate);
			nseIndices.add(indices);
		}
		return nseIndices;
	}

	public static NSEVolatility nseVolatilities(String jsonData) {
		String HEADERS = "Date,Symbol,Underlying Close Price (A),Underlying Previous Day Close Price (B),Underlying Log Returns (C) = LN(A/B),Previous Day Underlying Volatility (D),Current Day Underlying Daily Volatility (E) = Sqrt(0.995*D*D + 0.005*C*C),Underlying Annualised Volatility (F) = E*Sqrt(365)";
		if (JsonPathUtils.isEmpty(jsonData) || HEADERS.equalsIgnoreCase(jsonData)) {
			return null;
		}
		String[] cell = jsonData.split(",");
		NSEVolatility nseVolatility = new NSEVolatility();
		nseVolatility.setTradeDate(DateUtils.getDate(cell[0], DATE_FORMAT));
		nseVolatility.setSymbol(cell[1]);
		nseVolatility.setClose(JsonPathUtils.getDouble(cell[2]));
		nseVolatility.setPrevClose(JsonPathUtils.getDouble(cell[3]));
		nseVolatility.setLogReturn(JsonPathUtils.getDouble(cell[4]));
		nseVolatility.setPrevVolatility(JsonPathUtils.getDouble(cell[5]));
		nseVolatility.setDailyVolatility(JsonPathUtils.getDouble(cell[6]));
		nseVolatility.setYearVolatility(JsonPathUtils.getDouble(cell[7]));
		return nseVolatility;
	}

	public static List<NSEComplaint> nseInvestorComplaint(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEComplaint> complaints = new ArrayList<NSEComplaint>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.data");
		for (int i = 0; i < length; i++) {
			NSEComplaint complaint = new NSEComplaint();
			complaint.setCompanyName(response.read("$.data[" + i + "].companyName", String.class));
			complaint.setBeg(response.read("$.data[" + i + "].complBeg", Double.class));
			complaint.setRecv(response.read("$.data[" + i + "].complRecv", Double.class));
			complaint.setDisp(response.read("$.data[" + i + "].complDisp", Double.class));
			complaint.setUnres(response.read("$.data[" + i + "].complUnres", Double.class));
			String tradeDate = response.read("$.data[" + i + "].date", String.class);
			complaint.setComplaintUpdateDate(DateUtils.getDate(tradeDate, DATE_FORMAT));
			complaints.add(complaint);
		}
		return complaints;
	}

	public static List<NSEInsider> nseInsider(String jsonData) {
		if (JsonPathUtils.isEmpty(jsonData)) {
			return null;
		}
		List<NSEInsider> nseInsiders = new ArrayList<NSEInsider>();
		DocumentContext response = JsonPathUtils.jsonData(jsonData);
		int length = JsonPathUtils.length(response, "$.data");
		for (int i = 0; i < length; i++) {
			NSEInsider nseInsider = new NSEInsider();
			nseInsider.setAcqMode(response.read("$.data[" + i + "].acqMode", String.class));
			nseInsider.setAcqName(response.read("$.data[" + i + "].acqName", String.class));
			nseInsider.setAcqfromDt(response.read("$.data[" + i + "].acqfromDt", String.class));
			nseInsider.setAcqtoDt(response.read("$.data[" + i + "].acqtoDt", String.class));
			nseInsider.setAfterAcqSharesNo(response.read("$.data[" + i + "].afterAcqSharesNo", String.class));
			nseInsider.setAfterAcqSharesPer(response.read("$.data[" + i + "].afterAcqSharesPer", String.class));
			nseInsider.setAnex(response.read("$.data[" + i + "].anex", String.class));
			nseInsider.setBefAcqSharesNo(response.read("$.data[" + i + "].befAcqSharesNo", String.class));
			nseInsider.setBefAcqSharesPer(response.read("$.data[" + i + "].befAcqSharesPer", String.class));
			nseInsider.setBuyQuantity(response.read("$.data[" + i + "].buyQuantity", String.class));
			nseInsider.setBuyValue(response.read("$.data[" + i + "].buyValue", String.class));
			nseInsider.setCompanyName(response.read("$.data[" + i + "].company", String.class));
			nseInsider.setDerivativeType(response.read("$.data[" + i + "].derivativeType", String.class));
			nseInsider.setExchange(response.read("$.data[" + i + "].exchange", String.class));
			nseInsider.setIntimDt(response.read("$.data[" + i + "].intimDt", String.class));
			nseInsider.setPersonCategory(response.read("$.data[" + i + "].personCategory", String.class));
			nseInsider.setRemarks(response.read("$.data[" + i + "].remarks", String.class));
			nseInsider.setSecAcq(response.read("$.data[" + i + "].secAcq", String.class));
			nseInsider.setSecuritiesTypePost(response.read("$.data[" + i + "].securitiesTypePost", String.class));
			nseInsider.setSymbol(response.read("$.data[" + i + "].symbol", String.class));
			nseInsider.setTdpTransactionType(response.read("$.data[" + i + "].tdpTransactionType", String.class));
			nseInsider.setXbrl(response.read("$.data[" + i + "].xbrl", String.class));
			nseInsider.setSellQuantity(response.read("$.data[" + i + "].sellquantity", String.class));
			nseInsider.setSellValue(response.read("$.data[" + i + "].sellValue", String.class));
			nseInsider.setSecVal(response.read("$.data[" + i + "].secVal", String.class));
			nseInsider.setPid(response.read("$.data[" + i + "].pid", String.class));
			nseInsider.setSecType(response.read("$.data[" + i + "].secType", String.class));
			nseInsiders.add(nseInsider);
		}
		return nseInsiders;
	}

	public static NSEIndexEquity nseIndexEquities(String jsonData) {
		String HEADERS = "SYMBOL, SERIES, DATE1, PREV_CLOSE, OPEN_PRICE, HIGH_PRICE, LOW_PRICE, LAST_PRICE, CLOSE_PRICE, AVG_PRICE, TTL_TRD_QNTY, TURNOVER_LACS, NO_OF_TRADES, DELIV_QTY, DELIV_PER";
		if (JsonPathUtils.isEmpty(jsonData) || HEADERS.equalsIgnoreCase(jsonData)) {
			return null;
		}
		String[] cell = jsonData.split(",");
		NSEIndexEquity nseEquity = new NSEIndexEquity();
		nseEquity.setNosTrade(JsonPathUtils.getDouble(cell[12]));
		nseEquity.setDeliverableQty(JsonPathUtils.getDouble(cell[13]));
		nseEquity.setPerdeliverable(JsonPathUtils.getDouble(cell[14]));

		nseEquity.setPrevClose(JsonPathUtils.getDouble(cell[3]));
		nseEquity.setOpen(JsonPathUtils.getDouble(cell[4]));
		nseEquity.setHigh(JsonPathUtils.getDouble(cell[5]));
		nseEquity.setLow(JsonPathUtils.getDouble(cell[6]));
		nseEquity.setLtp(JsonPathUtils.getDouble(cell[7]));
		nseEquity.setClose(JsonPathUtils.getDouble(cell[8]));
		nseEquity.setAvg(JsonPathUtils.getDouble(cell[9]));

		nseEquity.setTradeQty(JsonPathUtils.getDouble(cell[10]));
		nseEquity.setTurnValue(JsonPathUtils.getDouble(cell[11]));
		nseEquity.setSymbol(cell[0]);
		nseEquity.setSeries(cell[1]);
		nseEquity.setTradeDate(DateUtils.getDate(cell[2], DATE_FORMAT));
		return nseEquity;
	}
}
