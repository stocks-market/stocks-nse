package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class NSEVolatility implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4980213063868347804L;
	
	private Integer id;
	
	private String symbol;
	
	private Date tradeDate;
	
	private Double close;
	
	private Double prevClose;
	
	private Double logReturn;
	
	private Double dailyVolatility;
	
	private Double prevVolatility;
	
	private Double yearVolatility;
}
