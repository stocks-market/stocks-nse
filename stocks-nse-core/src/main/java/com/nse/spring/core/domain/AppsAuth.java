package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(of = { "id" })
@Table(name = "APP_AUTH")
@JsonInclude(value = Include.NON_NULL)
public class AppsAuth implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1220168865548595958L;
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;
	
	@Column(name = "KEY")
	private String key;
	
	@Column(name = "VALUE")
	private byte[] value;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "ACTIVE")
	private boolean active;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
}
