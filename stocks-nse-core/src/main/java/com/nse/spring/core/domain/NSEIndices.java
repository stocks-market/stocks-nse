package com.nse.spring.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Entity
@Table(name = "NSE_INDICES")
@Data
@EqualsAndHashCode(of = { "symbol", "name", "indices" })
@JsonInclude(value = Include.NON_NULL)
public class NSEIndices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -446385087490587534L;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "ID")
	private String id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SYMBOL")
	private String symbol;
	
	@Column(name = "INDICES")
	private String indices;
}
