package com.nse.spring.core.utils;

import static com.nse.spring.core.utils.NSEContants.NSE_JWT_AUDIENCE;
import static com.nse.spring.core.utils.NSEContants.NSE_JWT_ISSUER;

import java.util.Date;
import java.util.UUID;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class TokenUtils {

	public static String nseToken(String jwtId, String keys) {
		String algoKey = keys == null ? UUID.randomUUID().toString() : keys;
		String jwtToken = JWT.create().withIssuedAt(new Date()).withExpiresAt(DateUtils.addHours(new Date(), 1))
				.withIssuer(NSE_JWT_ISSUER).withAudience(NSE_JWT_AUDIENCE).sign(Algorithm.HMAC256(algoKey));
		return jwtToken;
	}

	public static String nseToken() {
		return nseToken(null, null);
	}
}
