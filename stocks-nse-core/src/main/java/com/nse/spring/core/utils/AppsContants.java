package com.nse.spring.core.utils;

import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.nse.spring.core.domain.AppsAuth;

public class AppsContants {

	public static final String APPS_NSE = "NSE";

	public static final String APPS_BSE = "BSE";

	public static final String APPS_KITE = "KITE";
	
	public static final Integer APPS_NSE_TIME = 30;

	public static MultiValueMap<String, String> appsCookie(List<AppsAuth> appsAuths) {
		MultiValueMap<String, String> cookies = new LinkedMultiValueMap<String, String>();
		if (CollectionUtils.isEmpty(appsAuths)) {
			return cookies;
		}
		for(AppsAuth appsAuth: appsAuths) {
			String value = appsAuth.getValue() != null? new String(appsAuth.getValue()): null;
			cookies.add(appsAuth.getKey(), value);
		}
		return cookies;
	}
}
