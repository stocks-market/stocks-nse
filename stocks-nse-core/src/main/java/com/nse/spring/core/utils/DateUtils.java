package com.nse.spring.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.util.StringUtils;

public class DateUtils {
	public static Date getDate(String source, String format) {
		if (StringUtils.isEmpty(source) || StringUtils.isEmpty(format) || "-".equalsIgnoreCase(StringUtils.trimAllWhitespace(source))) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return dateFormat.parse(source);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static Long diffMinute(Date startDate, Date endDate) {
		if(startDate == null || endDate == null || endDate.getTime() < startDate.getTime()) {
			return Long.valueOf(-1);
		}
		Long diff = endDate.getTime() - startDate.getTime();
		return diff/(60 * 1000) % 60; 
	}

	public static String getDate(Date source, String format) {
		if (source == null || format == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(source);
	}

	public static Date addDate(Date source, int value) {
		if (source == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(source);
		calendar.add(Calendar.DATE, value);
		return calendar.getTime();
	}
	
	public static Date addHours(Date source, int value) {
		if (source == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(source);
		calendar.add(Calendar.HOUR, value);
		return calendar.getTime();
	}

	public static Date addMonth(Date source, int value) {
		if (source == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(source);
		calendar.add(Calendar.MONTH, value);
		return calendar.getTime();
	}
}
