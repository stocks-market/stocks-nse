package com.nse.spring.core.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = { "complaintUpdateDate" })
@JsonInclude(value = Include.NON_NULL)
public class NSEComplaint implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 30552995201322986L;

	private Integer id;
	
	private String symbol;

	private String companyName;

	private Double beg;

	private Double disp;

	private Double recv;

	private Double unres;

	private Date complaintUpdateDate;
}
