package com.nse.spring.core.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class NSEBrokers implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6149926847133058710L;
	
	private Integer id;
	
	private String symbol;
	
	private String exchange;
	
	private String series;
	
	private String brokerId;
	
	private String brokerName;
}
