package com.nse.spring.core.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nse.spring.core.domain.AppsAuth;

public interface AppsAuthRepository extends JpaRepository<AppsAuth, Serializable> {

	List<AppsAuth> findByNameAndActiveTrue(String name);
}
