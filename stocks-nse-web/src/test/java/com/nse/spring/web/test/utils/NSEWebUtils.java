package com.nse.spring.web.test.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class NSEWebUtils {
	
	public static final String APPLICATION_FILE_PATH = "./src/main/resources/application.properties";
	
	public static Map<String, String> load(String filePath) {
		Map<String, String> propsMap = new HashMap<String, String>();
		try (InputStream inStream = new FileInputStream(filePath)) {
			Properties properties = new Properties();
			properties.load(inStream);
			for(Map.Entry<Object, Object> entry: properties.entrySet()) {
				propsMap.put((String)entry.getKey(), (String)entry.getValue());
			}
		} catch(IOException ex) {}	
		return propsMap;
	}
}
