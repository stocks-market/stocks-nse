package com.nse.spring.web.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.nse.spring.core.domain.NSEComplaint;
import com.nse.spring.core.domain.NSEEvent;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.core.domain.NSEInsider;
import com.nse.spring.core.domain.NSEStocks;
import com.nse.spring.core.domain.NSEVolatility;
import com.nse.spring.core.repository.AppsAuthRepository;
import com.nse.spring.core.utils.DateUtils;
import com.nse.spring.web.test.utils.NSEWebUtils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@TestInstance(Lifecycle.PER_CLASS) // To avoid keeping `@BeforeAll` method as static
public class NSEStocksClientTest {

	NSEStocksClient nseStocksClient;
	
	@Autowired
	AppsAuthRepository appsAuthRepository;

	Environment env;

	@BeforeAll
	void setUp() {
		this.env = mock(Environment.class);
		initEnvironment();
		this.nseStocksClient = new NSEStocksClient(env, appsAuthRepository);
	}

	@Test
	@DisplayName(value = "REST Call for `NSEStocks`")
	public void testNseStock() {
		String symbol = "INDUSINDBK";
		String isIn = "INE095A01012";

		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("symbol", symbol);

		NSEStocks nseStock = new NSEStocks();
		nseStock.setSymbol("INDUSINDBK");
		nseStock.setIndustry("BANKS");
		nseStock.setName("IndusInd Bank Limited");
		nseStock.setIsIn(isIn);
		nseStock.setSeries("EQ");
		nseStock.setStatus("Listed");

		Mono<NSEStocks> nseStocksMono = this.nseStocksClient.nseStock(symbol);
		StepVerifier.create(nseStocksMono)
				.expectNextMatches(response -> nseStock.getSymbol().equalsIgnoreCase(response.getSymbol()))
				.verifyComplete();

		StepVerifier.create(nseStocksMono).thenConsumeWhile(ns -> {
			assertNotNull("NSEStocks Response can't be null", ns);
			assertEquals("NSEStocks Response Symbol match with " + symbol, ns.getSymbol(), symbol);
			assertEquals("NSEStocks Response Symbol match with " + symbol, ns.getIsIn(), isIn);
			return true;
		}).verifyComplete();
	}

	@Test
	@DisplayName(value = "REST Call for `NSEIndices`")
	public void testNseIndices() {
		Flux<NSEIndicesMeta> nseIndicesFlux = this.nseStocksClient.nseIndices();

		StepVerifier.create(nseIndicesFlux).expectNextMatches(ni -> "NIFTY 50".equalsIgnoreCase(ni.getSymbol()))
				.expectNextMatches(ni -> "NIFTY NEXT 50".equalsIgnoreCase(ni.getSymbol())).expectComplete();
	}

	@Test
	@DisplayName(value = "REST Call for `NSEIndexEquity`")
	public void testNseEquity() {
		Date tradeDate = DateUtils.getDate("28-08-2020", "dd-MM-yyyy");
		Flux<NSEIndexEquity> nseEquityFlux = this.nseStocksClient.nseEquity(DateUtils.getDate(tradeDate, "ddMMyyyy"));

		System.out.println("NSEEQUITY:\n");
		System.out.println(nseEquityFlux.blockLast());
	}

	@Test
	@DisplayName(value = "REST Call for `CorporateActions`")
	public void corporateActions() {
		Flux<NSEEvent> nseEventFlux = this.nseStocksClient.corporateActions("equities");

		System.out.println("NSEEQUITY:\n");
		System.out.println(nseEventFlux.blockLast());
	}

	@Test
	@DisplayName(value = "REST Call for `nseVolatility`")
	public void nseVolatility() {
		Flux<NSEVolatility> nseVolatilityFlux = this.nseStocksClient.nseVolatility("01092020");

		StepVerifier.create(nseVolatilityFlux).expectNextMatches(ni -> "20MICRONS".equalsIgnoreCase(ni.getSymbol()))
				.expectNextMatches(ni -> "21STCENMGM".equalsIgnoreCase(ni.getSymbol())).expectComplete();
	}

	@Test
	@DisplayName(value = "REST Call for `NSEInsider`")
	public void nseInsiders() {
		Flux<NSEInsider> nseInsiderFlux = this.nseStocksClient.nseInsiders("equities", "01-09-2020", "01-09-2020");

		StepVerifier.create(nseInsiderFlux).expectNextMatches(ni -> {
			assertEquals("MAJESCO", ni.getSymbol());
			assertEquals("ESOP", ni.getAcqMode());
			assertEquals("DIPEN GIRISH KANABAR", ni.getAcqName());
			return true;
		}).expectComplete();
	}

	@Test
	@DisplayName(value = "REST Call for `NSEComplaint`")
	public void nseInvestorComplaint() {
		Flux<NSEComplaint> nseComplaintFlux = this.nseStocksClient.nseInvestorComplaint("equities");

		StepVerifier.create(nseComplaintFlux).expectNextMatches(ni -> {
			return true;
		}).expectComplete();
	}

	private void initEnvironment() {
		Mockito.when(env.getProperty(anyString())).then(answer -> {
			String argument = String.valueOf(answer.getArguments()[0]);
			return argumentCase(argument);
		});
	}

	private String argumentCase(String key) {
		Map<String, String> propsMap = NSEWebUtils.load(NSEWebUtils.APPLICATION_FILE_PATH);
		return propsMap.getOrDefault(key, null);
	}
}
