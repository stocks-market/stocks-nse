package com.nse.spring.web.perf.simulation;

import io.gatling.core.Predef._;
import io.gatling.http.Predef._;
import scala.concurrent.duration._;
import io.gatling.core.scenario.Simulation;
import io.gatling.commons.stats.assertion.Is;

//mvn gatling:test -P perf -Dgatling.simulationClass=com.nse.spring.web.perf.simulation.NSEStocksSimulation

class NSEStocksSimulation extends Simulation {
  
  val httpProtocol = http.baseUrl("http://localhost:8080")
                         .acceptHeader("application/json")
                         .userAgentHeader("Gatling");
   val scn = scenario("NSEStocksSimulation")
    .repeat(10) {
      exec(http("GET /stocks").get("/stocks?symbol=SBIN").check(status.is(200), status.not(404), status.not(503), status.not(500)));
    }

  setUp(
    scn.inject(atOnceUsers(100))).protocols(httpProtocol)
}