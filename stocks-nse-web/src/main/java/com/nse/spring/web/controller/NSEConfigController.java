package com.nse.spring.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nse.spring.core.domain.AppsAuth;
import com.nse.spring.web.service.NSEConfigService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Component
@RestController
@RequestMapping(path = { "cookies" })
@RequiredArgsConstructor
public class NSEConfigController {

	@Autowired
	private Environment env;

	@Autowired
	private NSEConfigService nseConfigService;

	@GetMapping(path = { "/nse" })
	public Flux<AppsAuth> nseCookie() {
		return nseConfigService.nseCookie(env.getProperty("stocks.nse.baseUrl"));
	}
}
