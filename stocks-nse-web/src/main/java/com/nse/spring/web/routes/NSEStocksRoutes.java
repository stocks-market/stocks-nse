package com.nse.spring.web.routes;

import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.nse.spring.web.handler.NSEStocksHandler;

@Configuration
public class NSEStocksRoutes {

	@Bean
	public RouterFunction<ServerResponse> stocks(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/stocks", stocksHandler::nseStock).build();
	}

	@Bean
	public RouterFunction<ServerResponse> indexNames(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/indices", stocksHandler::nseIndices).build();
	}

	@Bean
	public RouterFunction<ServerResponse> equity(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/equity", stocksHandler::nseEquity).build();
	}

	@Bean
	public RouterFunction<ServerResponse> corporateActions(RouteLocatorBuilder builder,	NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/corporateActions", stocksHandler::corporateActions).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> stocksCorporateActions(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/stocks-corporateActions", stocksHandler::stocksCorporateActions).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> nseVolatility(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/volatility", stocksHandler::nseVolatility).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> nseInsiders(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/insiders", stocksHandler::nseInsiders).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> stocksInsiders(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/stocks-insiders", stocksHandler::stocksInsiders).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> indicesStockEquity(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/indices-stocks", stocksHandler::nseStockIndices).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> smeIndicesStocks(RouteLocatorBuilder builder, NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/indices-sme-stocks", stocksHandler::smeStockIndices).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> stocksDirectors(RouteLocatorBuilder builder,	NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/stocks-directors", stocksHandler::stocksDirectors).build();
	}
	
	@Bean
	public RouterFunction<ServerResponse> nseInvestorComplaint(RouteLocatorBuilder builder,	NSEStocksHandler stocksHandler) {
		return RouterFunctions.route().GET("/investor-complaint", stocksHandler::nseInvestorComplaint).build();
	}
}
