package com.nse.spring.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndices;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.web.service.NSEIndicesService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Component
@RestController
@RequestMapping(path = { "nse" })
@RequiredArgsConstructor
public class NSEIndicesController {

	@Autowired
	private NSEIndicesService nseIndicesService;

	@PostMapping(path = { "equity-indices" })
	public Flux<EquityIndicesStocks> equityIndices(@RequestBody List<EquityIndicesStocks> nseIndices) {
		return nseIndicesService.saveEquityIndices(nseIndices);
	}
	
	@PostMapping(path = { "indices-Stocks" })
	public Flux<NSEIndexEquity> nseIndicesStocks(@RequestBody List<NSEIndexEquity> nseIndices) {
		return nseIndicesService.nseIndicesStocks(nseIndices);
	}

	@PostMapping(path = { "indices" })
	public Flux<NSEIndices> filterIndices(@RequestBody List<NSEIndices> nseIndices) {
		return nseIndicesService.filterIndices(nseIndices);
	}

	@GetMapping(path = { "indices" })
	public List<NSEIndices> fetchIndices() {
		return nseIndicesService.fetchIndices();
	}

	@PostMapping(path = { "indices-meta" })
	public Flux<NSEIndicesMeta> filterIndicesMeta(@RequestBody List<NSEIndicesMeta> nseIndicesMetas) {
		return nseIndicesService.filterIndicesMeta(nseIndicesMetas);
	}

	@GetMapping(path = { "indices-meta" })
	public List<NSEIndicesMeta> fetchIndicesMetaByTradeDate(
			@RequestParam(name = "tradeDate", required = false) String tradeDate) {
		if (tradeDate == null) {
			return nseIndicesService.fetchIndicesMeta();
		}
		return nseIndicesService.fetchIndicesMetaByTradeDate(tradeDate);
	}
}
