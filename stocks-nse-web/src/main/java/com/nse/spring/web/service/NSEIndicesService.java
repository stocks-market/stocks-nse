package com.nse.spring.web.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndices;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.core.repository.EquityIndicesStocksRepository;
import com.nse.spring.core.repository.NSEIndexEquityRepository;
import com.nse.spring.core.repository.NSEIndicesMetaRepository;
import com.nse.spring.core.repository.NSEIndicesRepository;
import com.nse.spring.core.utils.DateUtils;
import com.nse.spring.core.utils.NSEDomainUtils;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class NSEIndicesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NSEIndicesService.class);

	@Autowired
	private NSEIndicesRepository nseIndicesRepository;

	@Autowired
	private NSEIndicesMetaRepository nseIndicesMetaRepository;

	@Autowired
	private NSEIndexEquityRepository nseIndexEquityRepository;

	@Autowired
	private EquityIndicesStocksRepository equityIndicesStocksRepository;

	public Flux<NSEIndexEquity> nseIndicesStocks(List<NSEIndexEquity> indexEquities) {
		LOGGER.info("[nseIndicesStocks] with Argument [List<NSEIndexEquity>] | START");
		if (CollectionUtils.isEmpty(indexEquities) || diffNSEIndexEquityTradeDate(indexEquities)) {
			return Flux.empty();
		}
		Date tradeDate = indexEquities.stream().map(m -> m.getTradeDate()).distinct().findFirst().get();
		String indiceTradeDate = DateUtils.getDate(tradeDate, NSEDomainUtils.DATE_FORMAT);
		List<NSEIndexEquity> nseIndexEquities = this.nseIndexEquityRepository.findByTradeDate(indiceTradeDate);

		boolean matched = !CollectionUtils.isEmpty(nseIndexEquities)
				&& nseIndexEquities.stream().count() == indexEquities.stream().count();
		Flux<NSEIndexEquity> dbIndicesFlux = Flux
				.fromIterable(this.nseIndexEquityRepository.findByTradeDate(indiceTradeDate));
		Flux<NSEIndexEquity> nseIndicesFlux = Flux.fromStream(indexEquities.stream())
				.filter(indice -> !matched && !nseIndexEquities.stream().anyMatch(dbIndice -> dbIndice.equals(indice)))
				.map(indice -> this.nseIndexEquityRepository.save(indice)).switchIfEmpty(dbIndicesFlux);
		LOGGER.info("[nseIndicesStocks] with Argument [List<NSEIndexEquity>] | END");
		return Flux.zip(nseIndicesFlux, dbIndicesFlux, (a, b) -> b);
	}

	public Flux<NSEIndices> filterIndices(List<NSEIndices> nseIndices) {
		LOGGER.info("[filterIndices] with Argument [List<NSEIndices>] | START");
		if (CollectionUtils.isEmpty(nseIndices)) {
			return Flux.empty();
		}
		List<NSEIndices> dbIndices = this.nseIndicesRepository.findAll();
		boolean matched = !CollectionUtils.isEmpty(dbIndices)
				&& nseIndices.stream().count() == dbIndices.stream().count();
		Flux<NSEIndices> dbIndicesFlux = Flux.fromIterable(this.nseIndicesRepository.findAll());
		Flux<NSEIndices> nseIndicesFlux = Flux.fromStream(nseIndices.stream())
				.filter(indice -> !matched && !dbIndices.stream().anyMatch(dbIndice -> dbIndice.equals(indice)))
				.map(indice -> this.nseIndicesRepository.save(indice)).switchIfEmpty(dbIndicesFlux);
		LOGGER.info("[filterIndices] with Argument [List<NSEIndices>] | END");
		return Flux.zip(nseIndicesFlux, dbIndicesFlux, (a, b) -> b);
	}

	public List<NSEIndices> fetchIndices() {
		List<NSEIndices> indices = this.nseIndicesRepository.findAll();
		return indices;
	}

	public Flux<NSEIndicesMeta> filterIndicesMeta(List<NSEIndicesMeta> nseIndicesMetas) {
		if (CollectionUtils.isEmpty(nseIndicesMetas) || diffNSEIndicesMetaTradeDate(nseIndicesMetas)) {
			return Flux.empty();
		}
		Date tradeDate = nseIndicesMetas.stream().map(m -> m.getTradeDate()).distinct().findFirst().get();
		String indiceTradeDate = DateUtils.getDate(tradeDate, NSEDomainUtils.DATE_FORMAT);
		List<NSEIndicesMeta> dbIndicesMetas = fetchIndicesMetaByTradeDate(indiceTradeDate);

		boolean matched = !CollectionUtils.isEmpty(dbIndicesMetas)
				&& nseIndicesMetas.stream().count() == dbIndicesMetas.stream().count();
		Flux<NSEIndicesMeta> dbIndicesMetaFlux = Flux.fromIterable(fetchIndicesMetaByTradeDate(indiceTradeDate));
		Flux<NSEIndicesMeta> nseIndicesMetaFlux = Flux.fromStream(nseIndicesMetas.stream())
				.filter(indice -> !matched || !dbIndicesMetas.stream().anyMatch(dbIndice -> dbIndice.equals(indice)))
				.map(indice -> this.nseIndicesMetaRepository.save(indice)).switchIfEmpty(dbIndicesMetaFlux);
		return Flux.zip(nseIndicesMetaFlux, dbIndicesMetaFlux, (a, b) -> b);
	}

	public List<NSEIndicesMeta> fetchIndicesMetaByTradeDate(String tradeDate) {
		List<NSEIndicesMeta> indicesMetas = this.nseIndicesMetaRepository.findByTradeDate(tradeDate);
		return indicesMetas;
	}

	public List<NSEIndicesMeta> fetchIndicesMeta() {
		List<NSEIndicesMeta> indicesMetas = this.nseIndicesMetaRepository.findAll();
		return indicesMetas;
	}

	private boolean diffNSEIndicesMetaTradeDate(List<NSEIndicesMeta> indicesMetas) {
		Long indicesMetaCount = indicesMetas.stream().map(m -> m.getTradeDate()).distinct().count();
		return indicesMetaCount > 1 || indicesMetaCount < 1;
	}

	private boolean diffNSEIndexEquityTradeDate(List<NSEIndexEquity> indexEquities) {
		Long indicesMetaCount = indexEquities.stream().map(m -> m.getTradeDate()).distinct().count();
		return indicesMetaCount > 1 || indicesMetaCount < 1;
	}

	private boolean diffEquityIndicesStocksTradeDate(List<EquityIndicesStocks> indexEquities) {
		Long indicesMetaCount = indexEquities.stream().map(m -> m.getTradeDate()).distinct().count();
		return indicesMetaCount > 1 || indicesMetaCount < 1;
	}

	public Flux<EquityIndicesStocks> saveEquityIndices(List<EquityIndicesStocks> equityIndices) {
		LOGGER.info("[saveEquityIndices] with Argument [List<EquityIndicesStocks>] | START");
		if (CollectionUtils.isEmpty(equityIndices) || diffEquityIndicesStocksTradeDate(equityIndices)) {
			return Flux.empty();
		}
		Date tradeDate = equityIndices.stream().map(m -> m.getTradeDate()).distinct().findFirst().get();
		String indiceTradeDate = DateUtils.getDate(tradeDate, NSEDomainUtils.DATE_FORMAT);
		List<EquityIndicesStocks> nseIndexEquities = this.equityIndicesStocksRepository.findByTradeDate(indiceTradeDate);

		boolean matched = !CollectionUtils.isEmpty(nseIndexEquities)
				&& nseIndexEquities.stream().count() == equityIndices.stream().count();
		Flux<EquityIndicesStocks> dbIndicesFlux = Flux
				.fromIterable(this.equityIndicesStocksRepository.findByTradeDate(indiceTradeDate));
		Flux<EquityIndicesStocks> nseIndicesFlux = Flux.fromStream(equityIndices.stream())
				.filter(indice -> !matched && !nseIndexEquities.stream().anyMatch(dbIndice -> dbIndice.equals(indice)))
				.map(indice -> this.equityIndicesStocksRepository.save(indice)).switchIfEmpty(dbIndicesFlux);
		LOGGER.info("[saveEquityIndices] with Argument [List<EquityIndicesStocks>] | END");
		return Flux.zip(nseIndicesFlux, dbIndicesFlux, (a, b) -> b);
	}
}
