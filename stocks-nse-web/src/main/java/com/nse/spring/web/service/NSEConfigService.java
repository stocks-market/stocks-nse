package com.nse.spring.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nse.spring.core.domain.AppsAuth;
import com.nse.spring.core.repository.AppsAuthRepository;
import com.nse.spring.core.utils.RestFluxUtils;

import reactor.core.publisher.Flux;

@Service
public class NSEConfigService {

	@Autowired
	private AppsAuthRepository appsAuthRepository;

	public Flux<AppsAuth> nseCookie(String baseNSEUrl) {
		return RestFluxUtils.nseCookie(baseNSEUrl, appsAuthRepository);
	}
}
