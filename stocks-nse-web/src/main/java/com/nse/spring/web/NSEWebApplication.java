package com.nse.spring.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@EnableJpaRepositories(basePackages = { "com.nse.spring.core.repository" })
@EntityScan(basePackages = { "com.nse.spring.core.domain" })
@SpringBootApplication
@ComponentScan(basePackages = { "com.nse.spring.web", "com.nse.spring.core" })
public class NSEWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(NSEWebApplication.class, args);
	}

}
