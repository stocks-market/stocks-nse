package com.nse.spring.web.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;

import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEComplaint;
import com.nse.spring.core.domain.NSEEvent;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.core.domain.NSEInsider;
import com.nse.spring.core.domain.NSEStocks;
import com.nse.spring.core.domain.NSEStocksDirectors;
import com.nse.spring.core.domain.NSEVolatility;
import com.nse.spring.core.repository.AppsAuthRepository;
import com.nse.spring.core.utils.AppsContants;
import com.nse.spring.core.utils.NSEDomainUtils;
import com.nse.spring.core.utils.RestFluxUtils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class NSEStocksClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(NSEStocksClient.class);

	private Environment env;

	private AppsAuthRepository appsAuthRepository;

	public NSEStocksClient(Environment env, AppsAuthRepository appsAuthRepository) {
		this.env = env;
		this.appsAuthRepository = appsAuthRepository;
	}

	public Flux<EquityIndicesStocks> nseSMEStocks() {
		String nseSmeUrl = env.getProperty("stocks.nse.sme");
		LOGGER.info("Fetching data from [{}] with [{}]", nseSmeUrl, EquityIndicesStocks.class.getName());
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get().uri(nseSmeUrl).accept(MediaType.ALL)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				})
				.retrieve()
				.onStatus(p -> HttpStatus.UNAUTHORIZED.equals(p), clientResponse -> {
					RestFluxUtils.nseCookie(env.getProperty("stocks.nse.baseUrl"), appsAuthRepository).subscribe();
	                return Mono.error(new RuntimeException("UnAuthorized to access "+nseUrl));
				})
				.bodyToFlux(String.class).flatMap(jsonData -> {
					List<EquityIndicesStocks> indexEquities = NSEDomainUtils.equityIndicesStocks(jsonData, "SME");
					return CollectionUtils.isEmpty(indexEquities) ? Flux.empty()
							: Flux.just(indexEquities.toArray(new EquityIndicesStocks[indexEquities.size()]));
				});
	}

	public Flux<NSEStocksDirectors> nseStocksDirectors(String symbol) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("SYMBOL", symbol.toUpperCase());
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.info"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEStocksDirectors> nseStocksDirectors = NSEDomainUtils.stocksDirectors(jsonData);
					LOGGER.info("[NSEStocksDirectors] value: {}", nseStocksDirectors);
					return CollectionUtils.isEmpty(nseStocksDirectors) ? Flux.empty()
							: Flux.just(nseStocksDirectors.toArray(new NSEStocksDirectors[nseStocksDirectors.size()]));
				});
	}

	public Flux<NSEStocksDirectors> nseIndicesStocks(String symbol) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("SYMBOL", symbol);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.info"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEStocksDirectors> nseStocksDirectors = NSEDomainUtils.stocksDirectors(jsonData);
					LOGGER.info("[NSEStocksDirectors] value: {}", nseStocksDirectors);
					return CollectionUtils.isEmpty(nseStocksDirectors) ? Flux.empty()
							: Flux.just(nseStocksDirectors.toArray(new NSEStocksDirectors[nseStocksDirectors.size()]));
				});
	}

	public Mono<NSEStocks> nseStock(String symbol) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("SYMBOL", symbol);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.quote-equity"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent"))
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).accept(MediaType.APPLICATION_JSON)
				.retrieve().bodyToMono(String.class).flatMap(jsonData -> { // flatMap
																			// =
																			// Asynchronous
																			// AND
																			// map =
																			// synchronous
					NSEStocks nseStock = NSEDomainUtils.nseStock(jsonData);
					LOGGER.info("[nseStock] value: {}", nseStock);
					return nseStock == null ? Mono.empty() : Mono.just(nseStock);
				});
	}

	public Flux<NSEIndicesMeta> nseIndices() {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.allIndices"))
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent"))
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).accept(MediaType.APPLICATION_JSON)
				.retrieve().bodyToFlux(String.class).flatMap(jsonData -> { // flatMap
																			// =
																			// Asynchronous
																			// AND
																			// map =
																			// synchronous
					List<NSEIndicesMeta> nseIndices = NSEDomainUtils.nseIndicesMeta(jsonData);
					LOGGER.info("[nseIndices] value: {}", nseIndices);
					return CollectionUtils.isEmpty(nseIndices) ? Flux.empty()
							: Flux.just(nseIndices.toArray(new NSEIndicesMeta[nseIndices.size()]));
				});
	}

	public Flux<NSEIndexEquity> nseEquity(String tradeDate) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("TRADE_DATE", tradeDate);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.archives.url")).get()
				.uri(env.getProperty("stocks.nse.archives.bhav-data"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					NSEIndexEquity indexEquities = NSEDomainUtils.nseIndexEquities(jsonData);
					LOGGER.info("[NSEIndexEquity] value: {}", indexEquities);
					return indexEquities == null ? Flux.empty() : Flux.just(indexEquities);
				});
	}

	public Flux<NSEEvent> corporateActions(String index) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("INDEX", index);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.corporateActions"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEEvent> nseEvents = NSEDomainUtils.corporateActions(jsonData);
					LOGGER.info("[corporateActions] value: {}", nseEvents);
					return CollectionUtils.isEmpty(nseEvents) ? Flux.empty()
							: Flux.just(nseEvents.toArray(new NSEEvent[nseEvents.size()]));
				});
	}

	public Flux<EquityIndicesStocks> indicesStocks(String index) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("INDEX", index);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.equity-stockIndices"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<EquityIndicesStocks> indexEquities = NSEDomainUtils.equityIndicesStocks(jsonData, index);
					return CollectionUtils.isEmpty(indexEquities) ? Flux.empty()
							: Flux.just(indexEquities.toArray(new EquityIndicesStocks[indexEquities.size()]));
				});
	}

	public Flux<NSEEvent> stocksCorporateActions(String symbol) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("SYMBOL", symbol);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.info"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEEvent> nseEvents = NSEDomainUtils.stocksCorporateActions(jsonData);
					LOGGER.info("[stocksCorporateActions] value: {}", nseEvents);
					return CollectionUtils.isEmpty(nseEvents) ? Flux.empty()
							: Flux.just(nseEvents.toArray(new NSEEvent[nseEvents.size()]));
				});
	}

	public Flux<NSEInsider> nseInsiders(String symbol) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("SYMBOL", symbol);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.info"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEInsider> nseEvents = NSEDomainUtils.stocksInsiders(jsonData);
					LOGGER.info("[NSEInsider] value: {}", nseEvents);
					return CollectionUtils.isEmpty(nseEvents) ? Flux.empty()
							: Flux.just(nseEvents.toArray(new NSEInsider[nseEvents.size()]));
				});
	}

	public Flux<NSEVolatility> nseVolatility(String tradeDate) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("TRADE_DATE", tradeDate);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.archives.url")).get()
				.uri(env.getProperty("stocks.nse.archives.volatility"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					NSEVolatility nseVolatility = NSEDomainUtils.nseVolatilities(jsonData);
					LOGGER.info("[NSEVolatility] value: {}", nseVolatility);
					return nseVolatility == null ? Flux.empty() : Flux.just(nseVolatility);
				});
	}

	public Flux<NSEInsider> nseInsiders(String index, String startDate, String endDate) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("INDEX", index);
		urlParams.put("START_DATE", startDate);
		urlParams.put("END_DATE", endDate);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.corporate-insider-trading"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEInsider> nseInsiders = NSEDomainUtils.nseInsider(jsonData);
					LOGGER.info("[NSEInsider] value: {}", nseInsiders);
					return CollectionUtils.isEmpty(nseInsiders) ? Flux.empty()
							: Flux.just(nseInsiders.toArray(new NSEInsider[nseInsiders.size()]));
				});
	}

	public Flux<NSEComplaint> nseInvestorComplaint(String index) {
		MultiValueMap<String, String> nseCookies = AppsContants.appsCookie(appsAuthRepository.findByNameAndActiveTrue(AppsContants.APPS_NSE));
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("INDEX", index);
		return RestFluxUtils.webClient(env.getProperty("stocks.nse.baseUrl")).get()
				.uri(env.getProperty("stocks.nse.investor-complaints"), urlParams)
				.header(HttpHeaders.ACCEPT_LANGUAGE, env.getProperty("spring.webflux.accept-language"))
				.header(HttpHeaders.USER_AGENT, env.getProperty("spring.webflux.user-agent")).accept(MediaType.ALL)
				.cookies(cookies -> {
					if (!CollectionUtils.isEmpty(nseCookies)) {
						cookies.addAll(nseCookies);
					}
				}).retrieve().bodyToFlux(String.class)
				.flatMap(jsonData -> {
					List<NSEComplaint> nseComplaints = NSEDomainUtils.nseInvestorComplaint(jsonData);
					LOGGER.info("[NSEComplaint] value: {}", nseComplaints);
					return CollectionUtils.isEmpty(nseComplaints) ? Flux.empty()
							: Flux.just(nseComplaints.toArray(new NSEComplaint[nseComplaints.size()]));
				});
	}
}
