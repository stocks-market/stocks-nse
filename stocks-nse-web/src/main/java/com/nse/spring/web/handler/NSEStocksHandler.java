package com.nse.spring.web.handler;

import java.util.Optional;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEComplaint;
import com.nse.spring.core.domain.NSEEvent;
import com.nse.spring.core.domain.NSEIndexEquity;
import com.nse.spring.core.domain.NSEIndicesMeta;
import com.nse.spring.core.domain.NSEInsider;
import com.nse.spring.core.domain.NSEStocks;
import com.nse.spring.core.domain.NSEStocksDirectors;
import com.nse.spring.core.domain.NSEVolatility;
import com.nse.spring.core.utils.NSERegexUtils;
import com.nse.spring.web.client.NSEStocksClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class NSEStocksHandler {

	private NSEStocksClient nseStocksClient;

	public NSEStocksHandler(NSEStocksClient nseStocksClient) {
		this.nseStocksClient = nseStocksClient;
	}

	/*
	 * https://www.nseindia.com/api/quote-equity?symbol=M%26M
	 * https://www.nseindia.com/get-quotes/equity?symbol=M%26M
	 */
	public Mono<ServerResponse> nseStock(ServerRequest serverRequest) {
		Optional<String> symbolOpts = serverRequest.queryParam("symbol");
		if (!symbolOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, symbolOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Mono<NSEStocks> nseStocksMono = this.nseStocksClient.nseStock(symbolOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseStocksMono,
				NSEStocks.class);

	}

	/*
	 * https://www.nseindia.com/market-data/live-market-indices
	 * https://www.nseindia.com/api/allIndices
	 */
	public Mono<ServerResponse> nseIndices(ServerRequest serverRequest) {
		Flux<NSEIndicesMeta> nseIndicesMetaFlux = this.nseStocksClient.nseIndices();
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseIndicesMetaFlux,
				NSEIndicesMeta.class);

	}

	/*
	 * https://www.nseindia.com/market-data/live-equity-market?symbol=NIFTY%2050
	 * https://www.nseindia.com/api/equity-stockIndices?index=NIFTY%2050
	 */
	public Mono<ServerResponse> nseStockIndices(ServerRequest serverRequest) {
		Optional<String> symbolOpts = serverRequest.queryParam("symbol");
		if (!symbolOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, symbolOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<EquityIndicesStocks> nseIndexEquityFlux = this.nseStocksClient.indicesStocks(symbolOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseIndexEquityFlux,
				EquityIndicesStocks.class);

	}

	/*
	 * https://www.nseindia.com/market-data/live-equity-market?symbol=NIFTY%2050
	 * https://www.nseindia.com/api/equity-stockIndices?index=NIFTY%2050
	 */
	public Mono<ServerResponse> smeStockIndices(ServerRequest serverRequest) {
		Flux<EquityIndicesStocks> nseIndexEquityFlux = this.nseStocksClient.nseSMEStocks();
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseIndexEquityFlux,
				EquityIndicesStocks.class);

	}

	/*
	 * https://www.nseindia.com/market-data/live-market-indices
	 * https://www.nseindia.com/api/allIndices
	 */
	public Mono<ServerResponse> nseEquity(ServerRequest serverRequest) {
		Optional<String> tradeDateOpts = serverRequest.queryParam("tradeDate");
		if (!tradeDateOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_DATE_FORMAT_REGEX, tradeDateOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEIndexEquity> nseIndexEquityFlux = this.nseStocksClient.nseEquity(tradeDateOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseIndexEquityFlux,
				NSEIndexEquity.class);

	}

	/*
	 * https://www.nseindia.com/companies-listing/corporate-filings-actions
	 * https://www.nseindia.com/api/corporates-corporateActions?index={INDEX}
	 */
	public Mono<ServerResponse> corporateActions(ServerRequest serverRequest) {
		Optional<String> indexeOpts = serverRequest.queryParam("index");
		if (!indexeOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, indexeOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEEvent> nseEventFlux = this.nseStocksClient.corporateActions(indexeOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseEventFlux,
				NSEEvent.class);

	}

	/*
	 * https://www.nseindia.com/all-reports
	 * https://archives.nseindia.com/archives/nsccl/volt/CMVOLT_{TRADE_DATE}.csv
	 */
	public Mono<ServerResponse> nseVolatility(ServerRequest serverRequest) {
		Optional<String> tradeDateOpts = serverRequest.queryParam("tradeDate");
		if (!tradeDateOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_DATE_FORMAT_REGEX, tradeDateOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEVolatility> nseVolatilityFlux = this.nseStocksClient.nseVolatility(tradeDateOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseVolatilityFlux,
				NSEVolatility.class);

	}

	/*
	 * https://www.nseindia.com/companies-listing/corporate-filings-insider-trading
	 * https://www.nseindia.com/api/corporates-pit?
	 */
	public Mono<ServerResponse> nseInsiders(ServerRequest serverRequest) {
		Optional<String> indexOpts = serverRequest.queryParam("index");
		Optional<String> startDateOpts = serverRequest.queryParam("startDate");
		Optional<String> endDateOpts = serverRequest.queryParam("endDate");
		if (!startDateOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_DATE_FORMAT_REGEX, startDateOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEInsider> nseInsidersFlux = this.nseStocksClient.nseInsiders(indexOpts.get(), startDateOpts.get(),
				endDateOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseInsidersFlux,
				NSEInsider.class);

	}

	/*
	 * https://www.nseindia.com/get-quotes/equity?symbol={SYMBOL}
	 * https://www.nseindia.com/api/quote-equity?symbol={SYMBOL}&section=corp_info
	 */
	public Mono<ServerResponse> stocksInsiders(ServerRequest serverRequest) {
		Optional<String> symbolOpts = serverRequest.queryParam("symbol");
		if (!symbolOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, symbolOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEInsider> nseInsidersFlux = this.nseStocksClient.nseInsiders(symbolOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseInsidersFlux,
				NSEInsider.class);

	}

	/*
	 * https://www.nseindia.com/companies-listing/corporate-filings-investor-
	 * complaints https://www.nseindia.com/api/investor-complaints?
	 */
	public Mono<ServerResponse> nseInvestorComplaint(ServerRequest serverRequest) {
		Optional<String> indexOpts = serverRequest.queryParam("index");
		Optional<String> startDateOpts = serverRequest.queryParam("startDate");
		Optional<String> endDateOpts = serverRequest.queryParam("endDate");
		if (!startDateOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, indexOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEComplaint> nseComplaintFlux = this.nseStocksClient.nseInvestorComplaint(indexOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseComplaintFlux,
				NSEComplaint.class);

	}

	/*
	 * https://www.nseindia.com/get-quotes/equity?symbol={SYMBOL}
	 * https://www.nseindia.com/api/quote-equity?symbol={SYMBOL}&section=corp_info
	 */
	public Mono<ServerResponse> stocksCorporateActions(ServerRequest serverRequest) {
		Optional<String> symbolOpts = serverRequest.queryParam("symbol");
		if (!symbolOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, symbolOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEEvent> nseEventFlux = this.nseStocksClient.stocksCorporateActions(symbolOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseEventFlux,
				NSEEvent.class);

	}

	/*
	 * https://www.nseindia.com/get-quotes/equity?symbol={SYMBOL}
	 * https://www.nseindia.com/api/quote-equity?symbol={SYMBOL}&section=corp_info
	 */
	public Mono<ServerResponse> stocksDirectors(ServerRequest serverRequest) {
		Optional<String> symbolOpts = serverRequest.queryParam("symbol");
		if (!symbolOpts.isPresent()
				|| !NSERegexUtils.isRegex(NSERegexUtils.NSE_ALPHA_NUMERIC_REGEX, symbolOpts.get())) {
			return ServerResponse.notFound().build();
		}
		Flux<NSEStocksDirectors> nseStocksDirectorFlux = this.nseStocksClient.nseStocksDirectors(symbolOpts.get());
		return ServerResponse.ok().header("Content-Type", MediaType.APPLICATION_JSON_VALUE).body(nseStocksDirectorFlux,
				NSEStocksDirectors.class);

	}
}
