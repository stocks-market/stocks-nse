drop table if exists nse_indices cascade;
drop table if exists nse_indices_meta cascade;
drop table if exists nse_indicex_equity cascade;

create table nse_indices (
	id varchar(255) not null, 
	indices varchar(255), 
	name varchar(255), 
	symbol varchar(255), 
	primary key (id)
);

create table nse_indices_meta (
	id varchar(255) not null, 
	advance float8, 
	decline float8, 
	indices varchar(255), 
	ltp float8, 
	name varchar(255), 
	pb float8, 
	pe float8, 
	symbol varchar(255), 
	trade_date timestamp, 
	unchange float8, 
	variation float8, 
	primary key (id)
);

create table nse_indicex_equity (
	id varchar(255) not null, 
	avg_price float8, 
	close_price float8, 
	d_volatility float8, 
	deliverable_qty float8, 
	e_volatility float8, 
	ffmc float8, 
	high_price float8, 
	indices varchar(255), 
	log_return float8, 
	low_price float8, 
	ltp float8, 
	nos_trade float8, 
	open_price float8, 
	perdeliverable float8, 
	position_id int4, 
	prev_close float8, 
	series varchar(255), 
	symbol varchar(255), 
	time_frame varchar(255), 
	trade_date timestamp, 
	trade_qty float8, 
	turn_value float8, 
	primary key (id)
);

create table nse_indices_stocks (
	id varchar(255) not null, 
	equity varchar(255), 
	stock_index varchar(255), 
	indices varchar(255), 
	position_id int4, 
	series varchar(255), 
	trade_date timestamp, 
	primary key (id)
);
