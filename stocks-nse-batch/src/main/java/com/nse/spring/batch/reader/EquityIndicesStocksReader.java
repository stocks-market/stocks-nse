package com.nse.spring.batch.reader;

import static com.nse.spring.core.utils.NSEUtils.START_STATUS;
import static com.nse.spring.core.utils.NSEUtils.STOP_STATUS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.nse.spring.batch.utils.NSEBatchUtils;
import com.nse.spring.core.domain.EquityIndicesStocks;
import com.nse.spring.core.domain.NSEIndicesMeta;

@StepScope
@Component(value = "equity-indices-reader")
public class EquityIndicesStocksReader implements ItemReader<EquityIndicesStocks[]>, StepExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(EquityIndicesStocksReader.class);

	private final String STATUS_KEY = "sme-indicesStocks";

	private String baseUrl;

	private List<String> indicesStocks;

	private String indices;

	private ExecutionContext context;

	public EquityIndicesStocksReader(@Value(value = "${nse.stocks.web.baseUrl}") String baseUrl,
			@Value("#{'${nse.stocks.web.read.indices-stocks}'.split(',')}") List<String> indicesStocks,
			@Value("${nse.stocks.web.indices}") String indices) {
		this.baseUrl = baseUrl;
		this.indicesStocks = indicesStocks;
		this.indices = indices;
	}

	@Override
	public EquityIndicesStocks[] read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		List<EquityIndicesStocks> equityIndicesStocks = new ArrayList<EquityIndicesStocks>();
		boolean isStockIndices = false;
		List<NSEIndicesMeta> indicesMetas = nseIndicesMeta();
		for (String stocksUrl : indicesStocks) {
			StringBuilder urls = new StringBuilder();
			urls.append(baseUrl).append("/").append(stocksUrl);
			ResponseEntity<EquityIndicesStocks[]> response = null;
			if (isStockIndices && !CollectionUtils.isEmpty(indicesMetas)) {
				List<EquityIndicesStocks[]> indicesStocks = indicesMetas.stream().map(m -> responseEnquity(urls.toString(), m.getSymbol())).map(m -> m.getBody()).filter(p -> p!=null).collect(Collectors.toList());
				indicesStocks.stream().map(m -> equityIndicesStocks.addAll(Arrays.asList(m))).count();
			} else if(!isStockIndices) {
				response = responseEnquity(urls.toString(), null);
			}
			if (response != null && response.getBody() != null
					&& !CollectionUtils.isEmpty(Arrays.asList(response.getBody()))) {
				equityIndicesStocks.addAll(Arrays.asList(response.getBody()));
			}
			isStockIndices = true;
		}

		boolean nextExecutionFlag = START_STATUS.equalsIgnoreCase(context.getString(STATUS_KEY));
		context.put(STATUS_KEY, STOP_STATUS);

		if (nextExecutionFlag && !CollectionUtils.isEmpty(equityIndicesStocks)) {
			return equityIndicesStocks.toArray(new EquityIndicesStocks[equityIndicesStocks.size()]);
		}
		return null;
	}

	private ResponseEntity<EquityIndicesStocks[]> responseEnquity(String urls, String symbol) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<EquityIndicesStocks[]> response = null;
		if (symbol != null) {
			Map<String, String> urlParams = new HashMap<String, String>();
			urlParams.put("symbol", symbol);
			response = restTemplate.getForEntity(urls, EquityIndicesStocks[].class, urlParams);
		} else {
			response = restTemplate.getForEntity(urls, EquityIndicesStocks[].class);
		}
		return response;
	}

	private List<NSEIndicesMeta> nseIndicesMeta() {
		StringBuilder urls = new StringBuilder();
		urls.append(baseUrl).append("/").append(indices);

		ResponseEntity<NSEIndicesMeta[]> responseEntity = NSEBatchUtils.restTemplateNseIndicesMeta(urls.toString());
		if (responseEntity.getBody() != null) {
			return Arrays.asList(responseEntity.getBody());
		}
		return Collections.emptyList();
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.context = stepExecution.getExecutionContext();
		this.context.put(STATUS_KEY, START_STATUS);
		LOGGER.info("[{}] EquityIndicesStocks Step via Before StepListener", "STARTING");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.info("[{}] EquityIndicesStocks Step via After StepListener", stepExecution.getExitStatus());
		return stepExecution.getExitStatus();
	}

}
