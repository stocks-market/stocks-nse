package com.nse.spring.batch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nse.spring.batch.config.EquityIndicesStocksJobConfiguration;
import com.nse.spring.batch.config.NSEIndicesJobConfiguration;

@RestController
@RequestMapping(path = "jobs")
public class NSEJobConfiguration {

	@Autowired
	private NSEIndicesJobConfiguration nseIndicesJobConfiguration;

	@Autowired
	private EquityIndicesStocksJobConfiguration equityIndicesStocksJobConfiguration;

	@GetMapping(path = "indices")
	public String jobsNseIndices() {
		this.nseIndicesJobConfiguration.launchIndicesJob();
		this.equityIndicesStocksJobConfiguration.launchEquityIndicesJob();
		return "SUCCESS";
	}
}
