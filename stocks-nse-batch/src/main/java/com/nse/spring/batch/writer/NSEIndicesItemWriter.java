package com.nse.spring.batch.writer;

import java.util.Arrays;
import java.util.List;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.nse.spring.core.domain.NSEIndices;
import com.nse.spring.core.domain.NSEIndicesMeta;

@StepScope
@Component(value = "indices-writer")
public class NSEIndicesItemWriter implements ItemWriter<NSEIndicesMeta[]> {
	
	private String baseUrl;

	private String indices;

	public NSEIndicesItemWriter(@Value(value = "${nse.stocks.web.nse.baseUrl}") String baseUrl,
			@Value("${nse.stocks.web.indices-meta}") String indices) {
		this.baseUrl = baseUrl;
		this.indices = indices;
	}

	@Override
	public void write(List<? extends NSEIndicesMeta[]> indice) throws Exception {
		if (CollectionUtils.isEmpty(indice)) {
			return;
		}
		for(NSEIndicesMeta	[] indicesRequest: indice) {
			StringBuilder urls = new StringBuilder();
			urls.append(baseUrl).append("/").append(indices);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<NSEIndices[]> responseIndicesMeta = restTemplate.postForEntity(urls.toString(), Arrays.asList(indicesRequest),  NSEIndices[].class);
		}
	}

}
