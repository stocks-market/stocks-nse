package com.nse.spring.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nse.spring.core.domain.EquityIndicesStocks;

@Configuration
public class EquityIndicesStocksJobConfiguration {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	@Qualifier(value = "equity-indices-reader")
	private ItemReader<EquityIndicesStocks[]> equityIndicesReader;

	@Autowired
	@Qualifier(value = "equity-indices-writer")
	private ItemWriter<EquityIndicesStocks[]> equityIndicesWriter;

	@Bean(name = "step-equity_indices")
	public Step equityIndicesStep() {
		return stepBuilderFactory.get("STEP-EQUITY_INDICES_RW").listener(equityIndicesReader)
				.<EquityIndicesStocks[], EquityIndicesStocks[]>chunk(100).reader(equityIndicesReader)
				.writer(equityIndicesWriter).build();
	}

	@Bean(name = "job-equity_indices")
	public Job equityIndicesJob() {
		return this.jobBuilderFactory.get("JOB-EQUITY_INDICES_RW").start(equityIndicesStep()).build();
	}

	public void launchEquityIndicesJob() {
		JobParameters jobParameters = new JobParameters();
		try {
			this.jobLauncher.run(equityIndicesJob(), jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException e) {
			e.printStackTrace();
		}
	}
}
