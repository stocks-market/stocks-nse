package com.nse.spring.batch.utils;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.nse.spring.core.domain.NSEIndicesMeta;

public class NSEBatchUtils {

	public static final ResponseEntity<NSEIndicesMeta[]> restTemplateNseIndicesMeta(String url) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<NSEIndicesMeta[]> response = restTemplate.getForEntity(url, NSEIndicesMeta[].class);
		return response;
	}
}
