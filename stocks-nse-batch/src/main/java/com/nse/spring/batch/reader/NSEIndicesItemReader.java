package com.nse.spring.batch.reader;

import static com.nse.spring.core.utils.NSEUtils.START_STATUS;
import static com.nse.spring.core.utils.NSEUtils.STOP_STATUS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.nse.spring.batch.utils.NSEBatchUtils;
import com.nse.spring.core.domain.NSEIndicesMeta;

@StepScope
@Component(value = "indices-reader")
public class NSEIndicesItemReader implements ItemReader<NSEIndicesMeta[]>, StepExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(NSEIndicesItemReader.class);

	private final String STATUS_KEY = "indicesStatus";

	private String baseUrl;

	private String indices;

	private ExecutionContext context;

	public NSEIndicesItemReader(@Value(value = "${nse.stocks.web.baseUrl}") String baseUrl,
			@Value("${nse.stocks.web.indices}") String indices) {
		this.baseUrl = baseUrl;
		this.indices = indices;
	}

	@Override
	public NSEIndicesMeta[] read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		StringBuilder urls = new StringBuilder();
		urls.append(baseUrl).append("/").append(indices);

		ResponseEntity<NSEIndicesMeta[]> response = NSEBatchUtils.restTemplateNseIndicesMeta(urls.toString());

		boolean nextExecutionFlag = START_STATUS.equalsIgnoreCase(context.getString(STATUS_KEY));
		context.put(STATUS_KEY, STOP_STATUS);

		if (nextExecutionFlag) {
			return response.getBody();
		}
		return null;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.context = stepExecution.getExecutionContext();
		this.context.put(STATUS_KEY, START_STATUS);
		LOGGER.info("[{}] NSEIndices Step via StepListener", "STARTING");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.info("[{}] NSEIndices Step via StepListener with ExitStatus [{}]", "STARTING",
				stepExecution.getExitStatus().getExitCode());
		return stepExecution.getExitStatus();
	}
}
