package com.nse.spring.batch.writer;

import java.util.Arrays;
import java.util.List;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.nse.spring.core.domain.EquityIndicesStocks;

@StepScope
@Component(value = "equity-indices-writer")
public class EquityIndicesStocksWriter implements ItemWriter<EquityIndicesStocks[]> {

	private String baseUrl;

	private String indicesStocks;

	public EquityIndicesStocksWriter(@Value(value = "${nse.stocks.web.nse.baseUrl}") String baseUrl,
			@Value("${nse.stocks.web.write.indices-stocks}") String indicesStocks) {
		this.baseUrl = baseUrl;
		this.indicesStocks = indicesStocks;
	}

	@Override
	public void write(List<? extends EquityIndicesStocks[]> equityIndices) throws Exception {
		if (CollectionUtils.isEmpty(equityIndices)) {
			return;
		}
		for (EquityIndicesStocks[] indicesRequest : equityIndices) {
			StringBuilder urls = new StringBuilder();
			urls.append(baseUrl).append("/").append(indicesStocks);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<EquityIndicesStocks[]> responseIndicesMeta = restTemplate.postForEntity(urls.toString(),
					Arrays.asList(indicesRequest), EquityIndicesStocks[].class);
		}
	}

}
