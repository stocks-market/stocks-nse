package com.nse.spring.batch.config;

import java.util.Arrays;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.batch.item.support.builder.CompositeItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.nse.spring.batch.reader.NSEIndicesItemReader;
import com.nse.spring.batch.tasklet.NSEWebAuthTasklet;
import com.nse.spring.core.domain.NSEIndicesMeta;

@Configuration
public class NSEIndicesJobConfiguration {

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private NSEWebAuthTasklet nseWebAuthTasklet;

	@Autowired
	private NSEIndicesItemReader nseIndicesReader;

	@Autowired
	@Qualifier(value = "composite-indices-writer")
	private ItemStreamWriter<NSEIndicesMeta[]> compositeIndicesWriter;

	@Bean(name = "composite-indices-writer")
	public CompositeItemWriter<NSEIndicesMeta[]> nseIndicesWriter(
			@Qualifier(value = "indices-meta-writer") ItemWriter<NSEIndicesMeta[]> indicesMetaItemWriter,
			@Qualifier(value = "indices-writer") ItemWriter<NSEIndicesMeta[]> indicesItemWriter) {
		CompositeItemWriterBuilder<NSEIndicesMeta[]> writerBuilder = new CompositeItemWriterBuilder<NSEIndicesMeta[]>();
		writerBuilder.delegates(Arrays.asList(indicesMetaItemWriter, indicesItemWriter));
		CompositeItemWriter<NSEIndicesMeta[]> compositeItemWriter = writerBuilder.build();
		return compositeItemWriter;
	}

	@Bean(name = "indices_rw_step")
	public Step indicesStep() {
		return stepBuilderFactory.get("STEP-INDICES_RW").listener(nseIndicesReader)
				.<NSEIndicesMeta[], NSEIndicesMeta[]>chunk(100).reader(nseIndicesReader).writer(compositeIndicesWriter)
				.build();
	}

	@Bean
	public Job indicesJob() {
		return this.jobBuilderFactory.get("JOB-INDICES_RW")
				.start(stepBuilderFactory.get("STEP-NSEAUTH").tasklet(nseWebAuthTasklet).build()).next(indicesStep())
				.build();
	}

	public void launchIndicesJob() {
		JobParameters jobParameters = new JobParameters();
		try {
			this.jobLauncher.run(indicesJob(), jobParameters);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException e) {
			e.printStackTrace();
		}
	}
}
