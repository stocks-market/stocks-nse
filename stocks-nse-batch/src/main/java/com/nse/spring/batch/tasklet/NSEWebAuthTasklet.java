package com.nse.spring.batch.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class NSEWebAuthTasklet implements Tasklet, StepExecutionListener  {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NSEWebAuthTasklet.class);
	
	private Environment env;
	
	private ExecutionContext context;
	
	public NSEWebAuthTasklet(Environment env) {
		this.env = env;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		boolean retryFlag = env.getProperty("nse.stocks.batch.retry", Boolean.class);
		RestTemplate restTemplate = new RestTemplate();
		String nseUrl = env.getProperty("nse.stocks.web.baseUrl") + env.getProperty("nse.stocks.web.cookies");
		try {
			restTemplate.getForEntity(nseUrl, String.class);
		} catch(Exception ex) {
			LOGGER.error("Got Exception [{}] details on exception", ex.getClass(), ex.getMessage());
			if(retryFlag && this.context.getInt("retry.count") > 0) {
				LOGGER.warn("[{}] NSEWebAuthTasklet attempt Remaining: [{}] for URL: [{}]", "Retrying", context.getInt("retry.count"), nseUrl);
				context.putInt("retry.count", context.getInt("retry.count") - 1);
				return RepeatStatus.CONTINUABLE;
			}
		}
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.info("[{}] NSEWebAuthTasklet Step via After StepListener", stepExecution.getExitStatus());
		return stepExecution.getExitStatus();
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		this.context = stepExecution.getExecutionContext();
		this.context.putInt("retry.count", Integer.valueOf(env.getProperty("nse.stocks.batch.retry.count", "5")));
		LOGGER.info("[{}] NSEWebAuthTasklet Step via Before StepListener", "STARTING");
	}

}
